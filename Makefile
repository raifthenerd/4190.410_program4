CC = g++
CFLAGS = -Wall -g
INCLUDES =
LFLAGS =
LIBS = -lGL -lGLU -lglut

RM = rm -f
MKDIR = mkdir -p
CP = cp
ZIP = tar -zcvf

MAIN = program4-part2.out
SRCS = lodepng.cpp texture.cpp viewport.cpp main.cpp
OBJS = $(SRCS:.cpp=.o)
RESOURCES = parabolic.png

.PHONY: clean
all: $(MAIN)
$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
run: $(MAIN)
	./$(MAIN)
tar:
	$(MKDIR) $(MAIN:.out=)
	$(CP) $(RESOURCES) $(MAIN:.out=)
	$(CP) *.{cpp,h} $(MAIN:.out=) && $(CP) Makefile $(MAIN:.out=)
	@sleep 0.1
	$(ZIP) $(MAIN:.out=.tar.gz) $(MAIN:.out=)
	$(RM) -r $(MAIN:.out=)
clean:
	$(RM) *.o $(MAIN) $(MAIN:.out=.tar.gz)
