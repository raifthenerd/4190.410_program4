#include <algorithm>
#include <GL/freeglut.h>
#include <cstdio>
#include "texture.h"
#include "viewport.h"

#define M_PI 3.14159265358979323846

const int N = 7;  // (N+1) interpolating points
const int L = 3;  // interpolation depth
const int LOD = N*(1<<L);
const int C = 32; // 32-gone approximated circle
const int DIM = 3;
const float R = 100.0;

double circlePoints[C][DIM];
double controlPoints[3][N+1][DIM];
double intermediatePoints[LOD+1][DIM];
double surfacePoints[LOD+1][C][DIM];
double surfaceNormals[LOD+1][C][DIM];
double texCoords[LOD+1][C][2];

int selectedView, selectedPoint[2];
const int INIT_SIZE = 800;
int width = INIT_SIZE;
int height = INIT_SIZE;
bool is_animation = false;

int mouseButton = -1;
int lastX, lastY;
double radius, t;
Vector3d eye, center, upVector;

double Length(double x, double y, double z)
{
    return sqrt(x*x + y*y + z*z);
}

void Normalize(double* v)
{
    double l = Length(v[0], v[1], v[2]);
    if (l > 0)
        for (int i = 0; i < DIM; i++)
            v[i] /= l;
}

void InitializeCircleLocations()
{
    for (int i=0; i<C; ++i) {
        circlePoints[i][0] = cos(2.0*M_PI*i/C);
        circlePoints[i][1] = sin(2.0*M_PI*i/C);
        circlePoints[i][2] = 0;
    }
}

void EvaluateSurface()
{
    int i,j,k,diff,idx,tmp;
    double t_inv = 1.0 - t;
    double b0 = t_inv * t_inv;
    double b1 = 2.0 * t_inv * t;
    double b2 = t * t;
    diff = 1<<L;
    for (j=0; j<=N; ++j) {
        for (k=0; k<DIM; ++k) {
            intermediatePoints[j*diff][k] =
                b0*controlPoints[0][j][k] +
                b1*controlPoints[1][j][k] +
                b2*controlPoints[2][j][k];
        }
    }
    while (diff > 1) {
        j = diff/2;
        while (j <= LOD) {
            for (k=0; k<DIM; ++k) {
                intermediatePoints[j][k] =
                    intermediatePoints[j+diff/2][k]+
                    intermediatePoints[j-diff/2][k];
                intermediatePoints[j][k] *= 9.0;
                idx = j-3*(diff/2);
                idx = idx < 0 ? 0 : idx;
                intermediatePoints[j][k] -= intermediatePoints[idx][k];
                idx = j+3*(diff/2);
                idx = idx > LOD ? LOD : idx;
                intermediatePoints[j][k] -= intermediatePoints[idx][k];
                intermediatePoints[j][k] /= 16.0;
            }
            j += diff;
        }
        diff /= 2;
    }
    double prev[DIM], curr[DIM], rotateMatrix[DIM][DIM], tempMatrix[2][DIM][DIM];
    for (i=0; i<=LOD; ++i) {
        if (i==0) {
            prev[0] = 0; prev[1] = 0; prev[2] = 1.0;
            for (k=0; k<DIM; ++k) {
                curr[k] = intermediatePoints[i+1][k]-intermediatePoints[i][k];
            }
        } else if (i == LOD) {
            for (k=0; k<DIM; ++k) {
                prev[k] = curr[k];
                curr[k] = intermediatePoints[i][k]-intermediatePoints[i-1][k];
            }
        } else {
            for (k=0; k<DIM; ++k) {
                prev[k] = curr[k];
                curr[k] = intermediatePoints[i+1][k]-intermediatePoints[i-1][k];
            }
        }
        Normalize(prev); Normalize(curr);
        for (j=0; j<DIM; ++j) {
            for (k=0; k<DIM; ++k) {
                tempMatrix[0][j][k] = -2.0*prev[j]*prev[k];
                if (j==k) tempMatrix[0][j][k] += 1.0;
            }
        }
        for (k=0; k<DIM; ++k) prev[k] += curr[k];
        Normalize(prev);
        for (j=0; j<DIM; ++j) {
            for (k=0; k<DIM; ++k) {
                tempMatrix[1][j][k] = -2.0*prev[j]*prev[k];
                if (j==k) tempMatrix[1][j][k] += 1.0;
            }
        }
        for (j=0; j<DIM; ++j) {
            for (k=0; k<DIM; ++k) {
                rotateMatrix[j][k] = 0;
                for (tmp=0; tmp<DIM; ++tmp) {
                    rotateMatrix[j][k] +=
                        tempMatrix[0][k][tmp]*tempMatrix[1][tmp][j];
                }
            }
        }
        for (j=0; j<C; ++j) {
            for (k=0; k<DIM; ++k) {
                surfaceNormals[i][j][k] = 0;
                for (tmp=0; tmp<DIM; ++tmp) {
                    if (i==0) {
                        surfaceNormals[i][j][k] +=
                            rotateMatrix[k][tmp]*circlePoints[j][tmp];
                    }
                    else {
                        surfaceNormals[i][j][k] +=
                            rotateMatrix[k][tmp]*surfaceNormals[i-1][j][tmp];
                    }
                }
            }
            Normalize(surfaceNormals[i][j]);
            for (k=0; k<DIM; ++k) {
                surfacePoints[i][j][k] =
                    R*surfaceNormals[i][j][k]+intermediatePoints[i][k];
            }
        }
    }
}

void setFrameTime(double newt)
{
    is_animation = false;
    t = newt;
    EvaluateSurface();
}

void InitPoints()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j <= N; ++j) {
            switch (j%3) {
            case 0:
                controlPoints[i][j][0] = INIT_SIZE * (0.6+0.2*(i-1)-1.2*j/N);
                controlPoints[i][j][1] = INIT_SIZE * (0.4+0.3*(i-1)-0.8*j/N);
                controlPoints[i][j][2] = INIT_SIZE * (0.4-0.8*j/N);
                break;
            case 1:
                controlPoints[i][j][0] = INIT_SIZE * (0.4+0.3*(i-1)-0.8*j/N);
                controlPoints[i][j][1] = INIT_SIZE * (0.6+0.2*(i-1)-1.2*j/N);
                controlPoints[i][j][2] = INIT_SIZE * (0.4-0.8*j/N);
                break;
            case 2:
                controlPoints[i][j][0] = INIT_SIZE * (0.4+0.3*(i-1)-0.8*j/N);
                controlPoints[i][j][1] = INIT_SIZE * (0.4+0.3*(i-1)-0.8*j/N);
                controlPoints[i][j][2] = INIT_SIZE * (0.6+0.1*(i-1)-1.2*j/N);
                break;
            }
        }
    }
    t = 0;
    EvaluateSurface();
    selectedView = -1;
}

float thickness = 1.5;

void Init()
{
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition(300, 100);
    glutInitWindowSize(width, height);
    glutCreateWindow("2015 Fall Computer Graphics HW #3-4 Example");

    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_LINE_SMOOTH);
    glPointSize(6 * thickness);

    InitializeCircleLocations();
    InitPoints();

    unsigned int tex;
    int width, height;
    initPNG(&tex, "parabolic.png", width, height);


    eye = Vector3d(0, 0, 1000);
    center = Vector3d(0, 0, 0);
    upVector = Vector3d(0, 1, 0);
}

enum {FRONT, UP, LEFT, ROTATE};

void SelectViewport(int view, bool clear)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-width, width, -height, height, -100000, 100000);

    if (view == FRONT)
        gluLookAt(0, 0, 1, 0, 0, 0, 0, 1, 0);
    else if (view == UP)
        gluLookAt(0, 1, 0, 0, 0, 0, 0, 0, -1);
    else if (view == LEFT)
        gluLookAt(1, 0, 0, 0, 0, 0, 0, 1, 0);
    else
    {
        gluLookAt(eye.x, eye.y, eye.z, center.x, center.y, center.z, upVector.x, upVector.y, upVector.z);
    }

    int w = width / 2;
    int h = height / 2;
    int x = (view == LEFT || view == ROTATE)? w : 0;
    int y = (view == UP || view == ROTATE)? h : 0;
    glViewport(x, y, w, h);

    if (clear)
    {
        glScissor(x, y, w, h);
        glClearColor(view < 2? 0.9f : 1, view % 2 == 0? 0.9f : 1, view > 0 && view < 3? 0.9f : 1, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}

bool showAxes = false;

void DrawGrid()
{
    double length = INIT_SIZE / 2;

    glLineWidth(2 * thickness);
    glBegin(GL_LINES);
    
    if (showAxes)
        for (int axis = 0; axis < DIM; axis++)
        {
            glColor3f(axis == 0, axis == 1, axis == 2);
            glVertex3d(0, 0, 0);
            glVertex3d(length*(axis == 0), length*(axis == 1), length*(axis == 2));
        }

    glEnd();
}


void DrawSurface()
{
    for (int i = 0; i <= LOD; i++)
        for (int j = 0; j < C; j++)
        {
            // calculating reflecting vector
            Vector3d mapped;
            mapped.sub(center, eye);
            Vector3d n = Vector3d(surfaceNormals[i][j][0], surfaceNormals[i][j][1], surfaceNormals[i][j][2]);
            n.scale(-2 * n.dot(mapped));
            mapped.add(n);
            mapped.normalize();

            // for parabolic mapping
            if (mapped.z < 0) {
                texCoords[i][j][0] = (mapped.x/(-mapped.z+1)+1)/4;
                texCoords[i][j][1] = (-mapped.y/(-mapped.z+1)+1)/2;
            } else {
                texCoords[i][j][0] = (mapped.x/(mapped.z+1)+3)/4;
                texCoords[i][j][1] = (-mapped.y/(mapped.z+1)+1)/2;
            }
        }

    glEnable(GL_TEXTURE_2D);
    for (int i = 0; i < LOD; i++)
    {
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= C; j++)
        {
            glTexCoord2dv(texCoords[i][j%C]);
            glVertex3dv(surfacePoints[i][j%C]);
            glTexCoord2dv(texCoords[i+1][j%C]);
            glVertex3dv(surfacePoints[i+1][j%C]);
        }
        glEnd();
    }
    glDisable(GL_TEXTURE_2D);}

void DrawFrame()
{
    glLineWidth(1 * thickness);

    for (int i = 0; i < 3; ++i) {
        glBegin(GL_LINE_STRIP);
        glColor3f(0.6+0.2*(i==0), 0.6+0.2*(i==1), 0.6+0.2*(i==2));
        for (int j = 0; j <= N; ++j) {
            glVertex3dv(controlPoints[i][j]);
        }
        glEnd();
    }
    for (int j = 0; j <= N; ++j) {
        glBegin(GL_LINE_STRIP);
        glColor3f(0.8, 0.8, 0.8);
        for (int i = 0; i < 3; ++i) {
            glVertex3dv(controlPoints[i][j]);
        }
        glEnd();
    }

}

void DrawControlPoints()
{
    glDisable(GL_DEPTH_TEST);
    for (int i = 0; i < 3; ++i) {
        glBegin(GL_POINTS);
        glColor3f(0.4+0.4*(i==0), 0.4+0.4*(i==1), 0.4+0.4*(i==2));
        for (int j = 0; j <= N; ++j) {
            glVertex3dv(controlPoints[i][j]);
        }
        glEnd();
    }
    glEnable(GL_DEPTH_TEST);
}

void displayCallback()
{
    glEnable(GL_SCISSOR_TEST);
    for (int view = 0; view < 4; view++)
    {
        SelectViewport(view, true);
        if (view == ROTATE)
            DrawSurface();
        else
            DrawFrame();
        DrawGrid();
        if (view != ROTATE)
            DrawControlPoints();
    }
    glDisable(GL_SCISSOR_TEST);

    glutSwapBuffers();
}

void reshapeCallback(int nw, int nh)
{
    width = nw;
    height = nh;

    radius = sqrt(width * width + height * height) / 4;
}

void animate(int value)
{
    static float real_time = 0.0;
    if (is_animation) {
        real_time += 0.033;
        t = 0.5-0.5*exp(-0.1*real_time)*cos(5*real_time);
        EvaluateSurface();
        glutPostRedisplay();
        glutTimerFunc(33, animate, 0);
    } else {
        real_time = 0.0;
    }
}

void keyboardCallback(unsigned char key, int x, int y)
{
    if (key == 27)
        exit(0);
    else if (key == 'a' || key == 'A')
        showAxes = !showAxes;
    else if (key == '1')
        setFrameTime(0.0);
    else if (key == '2')
        setFrameTime(0.5);
    else if (key == '3')
        setFrameTime(1.0);
    else if (key == ' ') {
        is_animation = !is_animation;
        if (is_animation) {
            glutTimerFunc(33, animate, 0);
        }
    }
    glutPostRedisplay();
}

double ToLocalX(int x)
{
    return 4*x - width;
}

double ToLocalY(int y)
{
    return height - 4*y;
}

void mouseCallback(int button, int action, int x, int y)
{
    if (action == GLUT_DOWN)
    {
        if (x > width / 2 && y < height / 2)
        {
            selectedView = ROTATE;
            lastX = x - width / 2;
            lastY = y;
        }
        else
        {
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j <= N; ++j) {
                    int possibleView = -1;
                    double cx, cy;

                    if (x < width / 2)
                        if (y > height / 2)
                        {
                            possibleView = FRONT;
                            cx = controlPoints[i][j][0];
                            cy = controlPoints[i][j][1];
                        }
                        else
                        {
                            possibleView = UP;
                            cx = controlPoints[i][j][0];
                            cy = -controlPoints[i][j][2];
                        }
                    else if (y > height / 2)
                    {
                        possibleView = LEFT;
                        cx = -controlPoints[i][j][2];
                        cy = controlPoints[i][j][1];
                    }


                    if (possibleView > -1 && Length(cx - ToLocalX(x % (width / 2)), cy - ToLocalY(y % (height / 2)), 0) < 20)
                    {
                        selectedView = possibleView;
                        selectedPoint[0] = i;
                        selectedPoint[1] = j;
                    }
                }
            }
        }
        mouseButton = button;
    }
    else if (action == GLUT_UP)
    {
        selectedView = -1;
        mouseButton = -1;
    }
}

void motionCallback(int x, int y)
{
    if (selectedView == FRONT)
    {
        controlPoints[selectedPoint[0]][selectedPoint[1]][0] = ToLocalX(x);
        controlPoints[selectedPoint[0]][selectedPoint[1]][1] = ToLocalY(y - height/2);
    }
    else if (selectedView == UP)
    {
        controlPoints[selectedPoint[0]][selectedPoint[1]][0] = ToLocalX(x);
        controlPoints[selectedPoint[0]][selectedPoint[1]][2] = -ToLocalY(y);
    }
    else if (selectedView == LEFT)
    {
        controlPoints[selectedPoint[0]][selectedPoint[1]][2] = -ToLocalX(x - width/2);
        controlPoints[selectedPoint[0]][selectedPoint[1]][1] = ToLocalY(y - height/2);
    }
    else if (selectedView == ROTATE)
    {
        Vector3d lastP = getMousePoint(lastX, lastY, width / 2.0f, height / 2.0f, radius);
        Vector3d currentP = getMousePoint(x - width / 2.0f, y, width / 2.0f, height / 2.0f, radius);

        if (mouseButton == GLUT_LEFT_BUTTON)
        {
            Vector3d rotateVector;
            rotateVector.cross(currentP, lastP);
            double angle = -currentP.angle(lastP) * 2;
            rotateVector = unProjectToEye(rotateVector, eye, center, upVector);

            Vector3d dEye;
            dEye.sub(center, eye);
            dEye = rotate(dEye, rotateVector, -angle);
            upVector = rotate(upVector, rotateVector, -angle);
            eye.sub(center, dEye);
        }
        else if (mouseButton == GLUT_RIGHT_BUTTON) {
            Vector3d dEye;
            dEye.sub(center, eye);
            double offset = 0.025;
            if ((y - lastY) < 0) {
                dEye.scale(1 - offset);
            }
            else {
                dEye.scale(1 + offset);
            }
            eye.sub(center, dEye);
        }
        else if (mouseButton == GLUT_MIDDLE_BUTTON) {
            double dx = x - width / 2.0f - lastX;
            double dy = y - lastY;
            if (dx != 0 || dy != 0)
            {
                Vector3d moveVector(dx, dy, 0);
                moveVector = unProjectToEye(moveVector, eye, center, upVector);
                moveVector.normalize();
                double eyeDistance = Vector3d(eye).distance(Vector3d(center));
                moveVector.scale(sqrt(dx*dx + dy*dy) / 1000 * eyeDistance);
                center.add(moveVector);
                eye.add(moveVector);
            }
        }
        lastX = x - width / 2;
        lastY = y;
    }

    EvaluateSurface();
    glutPostRedisplay();
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    Init();
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(reshapeCallback);
    glutKeyboardFunc(keyboardCallback);
    glutMouseFunc(mouseCallback);
    glutMotionFunc(motionCallback);
    glutMainLoop();
    return 0;
}
